﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using KanekoUtilities;

//[RequireComponent(typeof(Rigidbody2D))]

public class GameManeger : SingletonMonoBehaviour<GameManeger>
{
    [SerializeField]
    MakeWall makeWall = null;

    [SerializeField]
    TouchBall player = null;

    [SerializeField]
    GameObject jouGe = null;

    [SerializeField]
    KaisuDisplay kaisu = null;

    [SerializeField]
    Transform wallContainer = null;

    [SerializeField]
    GameObject goaltext = null;

    [SerializeField]
    GameObject star = null;

    [SerializeField]
    TapTurorial tap = null;

    //Walls型
    [SerializeField]
    GameObject startposition = null;

    [SerializeField]
    Transform container = null;

    [SerializeField]
    TextMesh greattext = null;

    [SerializeField]
    GameObject failedtext = null;

    [SerializeField]
    ParticleSystem playereffect1 = null;
    [SerializeField]
    ParticleSystem playereffect2 = null;

    Vector3 plposition, jougeposition;

    public int starcnt { get; private set; }

    Color startColor;

    [SerializeField]
    Color boostColor = Color.white;

    protected override void Awake(){
        base.Awake();
        plposition = player.transform.position;
        jougeposition = jouGe.transform.position;
    }

    private void Start(){
        //playereffect1 = player.gameObject.transform.GetChild(1).GetComponent<ParticleSystem>();
        //playereffect2 = player.gameObject.transform.GetChild(2).GetComponent<ParticleSystem>();
        startColor = greattext.color;
        tap.gameObject.SetActive(true);
        StartCoroutine(GameTutorial());
    }

    //動詞スタート
    public void GameStart(){
        player.RigidBody2D.simulated = true;
        makeWall.StartGenerate();
    }

    public void GameFinish(){
        player.gameObject.SetActive(true);

        failedtext.SetActive(false);

        var module1 = playereffect1.main;
        module1.startColor = Color.white;
        var module2 = playereffect2.main;
        module2.startColor = Color.white;

        goaltext.gameObject.SetActive(false);

        starcnt = 0;

        player.transform.position = plposition;

        jouGe.transform.position = jougeposition;
        player.RigidBody2D.simulated = false;
        int num = wallContainer.childCount;
        for (int i = 0; i < num; i++){
            Destroy(wallContainer.GetChild(i).gameObject);
        }
        star.gameObject.SetActive(false);
        makeWall.stopFunc();
        //"StartPositionはconst
        GameObject startwall = (GameObject)Resources.Load("StartPosition");
        startposition = Instantiate(startwall, container);
        startposition.GetComponent<Walls>().enabled = false;
        kaisu.changeNum("0");
        player.kaisu = 0;
        Time.timeScale = 1.0f;
        Start();
    }

    private bool IsTapScreen(){
        if (Input.GetKeyDown(KeyCode.Space)){
            return true;
        }
        else if (Input.touchCount > 0){
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began){
                return true;
            }
        }
        return false;
    }

    IEnumerator GameTutorial()
    {
        yield return new WaitUntil(IsTapScreen);
        tap.gameObject.SetActive(false);
        startposition.GetComponent<Walls>().enabled = true;
        GameStart();
    }

    public void OnHitStar(bool isHit){
        if(!isHit){
            var module1 = playereffect1.main;
            module1.startColor = Color.white;
            var module2 = playereffect2.main;
            module2.startColor = Color.white;
            starcnt = 0;
        }else{
            starcnt++;

            if (starcnt == 5)
            {
                var module1 = playereffect1.main;
                module1.startColor = boostColor;
                var module2 = playereffect2.main;
                module2.startColor = boostColor;
                starcnt = 0;
                StartCoroutine(PlayTextAnimation());
            }
        }

    }
    //dotween
    IEnumerator PlayTextAnimation(){
        Color endColor = startColor;
        endColor.a = 0.0f;

        Vector2 playerpos = player.transform.position;
        Vector2 displayPos = playerpos;
        displayPos.x += 1.5f;
        displayPos.y += 0.5f;

        greattext.transform.position = displayPos;

        greattext.gameObject.SetActive(true);
        float scale = 0;
        yield return StartCoroutine(ScalableFloatLerp(0.5f, (t) => {
            scale = Mathf.Lerp(0.0f, 0.01f, Easing.OutQuad(t));
            greattext.gameObject.transform.localScale = Vector3.one * scale;
        }));
        yield return StartCoroutine(ScalableFloatLerp(0.5f, (t) => {
            greattext.color = Color.Lerp(startColor,endColor,Easing.Linear(t));
        }));
        greattext.gameObject.SetActive(false);
        greattext.color = startColor;
    }

    static IEnumerator ScalableFloatLerp(float duration, Action<float> action)
    {
        float t = 0.0f;

        while (true)
        {
            t += Time.deltaTime;
            action(t / duration);
            if (t > duration) break;
            yield return null;
        }

        action(1.0f);
    }
}
