﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarMove : MonoBehaviour {

    [SerializeField]
    float speed = 4.0f;

    public bool isHit = false;

    void Update()
    {
        transform.Translate(Vector3.left * speed * Time.deltaTime);
    }
}
