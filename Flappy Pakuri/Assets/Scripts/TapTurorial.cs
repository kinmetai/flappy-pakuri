﻿using System;
using System.Collections;
using UnityEngine;

public class TapTurorial : MonoBehaviour {

    private float scale = 0.0f;

	void OnEnable () {
        StartCoroutine(TapAnimation());
	}

    IEnumerator TapAnimation(){
        while(true){
            yield return StartCoroutine(ScalableFloatLerp(0.3f, (t) =>{
                scale = Mathf.Lerp(1.0f, 1.3f, t);
                this.transform.localScale = Vector3.one * scale;
            }));

            yield return StartCoroutine(ScalableFloatLerp(0.5f, (t) => {
                scale = Mathf.Lerp(1.3f, 1.0f, t);
                this.transform.localScale = Vector3.one * scale;
            }));

            yield return StartCoroutine(ScalableFloatLerp(0.5f, (t) => {
                scale = Mathf.Lerp(1.0f, 1.3f, t);
                this.transform.localScale = Vector3.one * scale;
            }));

            yield return StartCoroutine(ScalableFloatLerp(0.5f, (t) => {
                scale = Mathf.Lerp(1.3f, 1.0f, t);
                this.transform.localScale = Vector3.one * scale;
            }));
        }
        

    }

    static IEnumerator ScalableFloatLerp(float duration, Action<float> action){
        float t = 0.0f;

        while (true)
        {
            t += Time.deltaTime;
            action.Invoke(t / duration);
            if (t > duration) break;
            yield return null;
        }

        action.Invoke(1.0f);
    }
}
