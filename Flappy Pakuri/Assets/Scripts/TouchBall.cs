﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchBall : MonoBehaviour
{

    public int kaisu = 0;

    public Rigidbody2D RigidBody2D { get; private set; }
    [SerializeField]
    KaisuDisplay display = null;

    [SerializeField]
    Finish finish = null;

    [SerializeField]
    GameObject goaltext = null;

    [SerializeField]
    GameObject goaleffect = null;

    [SerializeField]
    GameObject playerdissapereffect = null;

    [SerializeField]
    Transform container = null;

    [SerializeField]
    GameObject failed = null;

    [SerializeField]
    TextMesh greatText = null;

    private float starttime;

    private float speed = 0.0f;

    ParticleSystem leftJetEffect = null;
    ParticleSystem rightJetEffect = null;

    float minangle = 0.0f;
    float maxangle = -15.0f;

    SpriteRenderer playersprite;
    [SerializeField]
    public Sprite player1;
    [SerializeField]
    public Sprite player2;


    // Use this for initialization
    void Start () {
		RigidBody2D = GetComponent<Rigidbody2D>();
        leftJetEffect = this.gameObject.transform.GetChild(1).gameObject.GetComponent<ParticleSystem>();
        rightJetEffect = this.gameObject.transform.GetChild(2).gameObject.GetComponent<ParticleSystem>();
        playersprite = this.transform.GetChild(0).GetComponent<SpriteRenderer>();
    }
	
	// Update is called once per frame
	void Update () {

        if(Input.GetKeyDown(KeyCode.Space)){
            starttime = Time.time;
            leftJetEffect.Play();
            rightJetEffect.Play();
            leftJetEffect.Emit(1);
            rightJetEffect.Emit(1);
            StopAllCoroutines();
            StartCoroutine(RotateAnimation(maxangle));
            StartCoroutine(PlayerAnimation());
        }
        else if(Input.GetKey(KeyCode.Space)){
            speed = (Time.time - starttime) * 10;
            RigidBody2D.velocity = transform.up * speed;
        } else if(Input.GetKeyUp(KeyCode.Space)){
            leftJetEffect.Stop();
            rightJetEffect.Stop();
            StopAllCoroutines();
            StartCoroutine(RotateAnimation(minangle));
        }


        if (Input.touchCount > 0) {
            Touch touch = Input.GetTouch(0);
            if(touch.phase == TouchPhase.Began){
                starttime = Time.time;
                leftJetEffect.Play();
                rightJetEffect.Play();
                leftJetEffect.Emit(1);
                rightJetEffect.Emit(1);
                StartCoroutine(RotateAnimation(maxangle));
                StartCoroutine(PlayerAnimation());
            } else if (touch.phase == TouchPhase.Stationary) {
                speed = (Time.time - starttime) * 10;
                RigidBody2D.velocity = transform.up * speed;
            } else if(touch.phase == TouchPhase.Moved) {
                speed = (Time.time - starttime) * 10;
                RigidBody2D.velocity = transform.up * speed;
            }else if (touch.phase == TouchPhase.Ended){
                leftJetEffect.Stop();
                rightJetEffect.Stop();
                StopAllCoroutines();
                StartCoroutine(RotateAnimation(minangle));
            }
        }
    }

    IEnumerator PlayerAnimation(){
        while (true)
        {
            yield return playersprite.sprite = player2;
            yield return playersprite.sprite = player1;
        }
    }

    IEnumerator RotateAnimation(float angle){
        float startangle = this.transform.localEulerAngles.z;
        if(startangle >= 180.0f){
            startangle -= 360.0f;
        } 
        float rotspeed = 50.0f;
        float duration = Mathf.Abs(angle - startangle) / rotspeed;

        Vector3 currentAngle = this.transform.localEulerAngles;

        float t = 0.0f;

        while(true){
            yield return null;
            t += Time.deltaTime;
            if (t > duration) break;
            currentAngle.z = Mathf.Lerp(startangle, angle, t / duration);
            this.transform.localEulerAngles = currentAngle;
        }

    }

    void OnCollisionEnter2D (Collision2D collision) {
        // 衝突判定
        if (!collision.gameObject.CompareTag("Walls")) return;
        if(greatText.gameObject.activeSelf){
            greatText.gameObject.SetActive(false);
        }

        Vector3 playerdissaper = this.transform.position;
        this.gameObject.SetActive(false);
        GameObject dronedis = (GameObject)Resources.Load("DroneDis");
        playerdissapereffect = Instantiate(dronedis, container);
        playerdissapereffect.transform.position = playerdissaper;
        playerdissapereffect.GetComponent<ParticleSystem>().Play();
        failed.SetActive(true);
        Time.timeScale = 0.0f;
        finish.gameObject.SetActive(true);


    }
	void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.tag == "kaisu"){
            kaisu++;
            string num = kaisu.ToString();
            display.changeNum(num);
        }else if (collision.gameObject.tag == "goal"){
            goaltext.gameObject.SetActive(true);
            finish.gameObject.SetActive(true);
            GameObject effectprefab = (GameObject)Resources.Load("PaperEffect");
            goaleffect = Instantiate(effectprefab, container);
            goaleffect.SetActive(true);
            goaleffect.GetComponent<ParticleSystem>().Play();
            Time.timeScale = 0.0f;
        }
	}
}
