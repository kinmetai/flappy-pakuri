﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walls : MonoBehaviour {
    [SerializeField]
    float speed = 4.0f;

    // Update is called once per frame
    void Update() {
        transform.Translate(Vector3.left * speed * Time.deltaTime);
        removeWall();
    }

    void removeWall() {
        if(this.gameObject.transform.position.x <= -4.0f){
            Destroy(this.gameObject);
        }
    }
}
