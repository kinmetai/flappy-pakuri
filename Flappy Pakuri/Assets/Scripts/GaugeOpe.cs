﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GaugeOpe : MonoBehaviour
{
    private Image bar;
    private float barpoint;
    [SerializeField]
    GameObject gimage = null;

    public void gaugeOpe(float point, float position){
        bar = this.GetComponent<Image>();
        barpoint = 1 - (point / position);
        bar.fillAmount = barpoint;
        if(barpoint >= 1.0f){
            Color color = new Color(5.0f/255.0f, 202.0f/255.0f, 18.0f/255.0f);
            gimage.GetComponent<Image>().color = color;
        }
    }

    public void gaugeInitialization() {
        bar = this.GetComponent<Image>();
        bar.fillAmount = 0;
        gimage.GetComponent<Image>().color = new Color(174.0f / 255.0f, 174.0f / 255.0f, 174.0f / 255.0f);
    }
}
