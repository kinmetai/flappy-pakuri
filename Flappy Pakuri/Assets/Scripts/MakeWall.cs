﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeWall : MonoBehaviour {
    enum Direction{ Down,UP }

    public float goalpos { get; private set; }
    public float length { get; private set; }
    public int wallcnt { get; private set; }
    private float previoius = 0.0f;

    public const int Max_Wall_Count = 30;
    public const float Min_Wall_Height = -2.1f;
    public const float Max_Wall_Height = 1.4f;
    public const int Start_Wall_Position = 5;

    private Direction direction = Direction.Down;

    [SerializeField]
    GameObject goalwall;

    [SerializeField]
    Transform container = null;

    [SerializeField]
    GaugeOpe gauge = null;

    GameObject prefabwall = null;
    GameObject prefabgoal = null;
    [SerializeField]
    GameObject prefabstar = null;

    SpriteRenderer uewallsprite;
    SpriteRenderer sitawallsprite;
    [SerializeField]
    public Sprite wall_color1;
    [SerializeField]
    public Sprite wall_color2;
    [SerializeField]
    public Sprite wall_color3;
    [SerializeField]
    public Sprite wall_color4;
    [SerializeField]
    public Sprite wall_color5;


    public void StartGenerate(){
        wallcnt = 0;
        createGoalWall();
        StartCoroutine(waitTime(0.35f));
    }


    private void Update(){
        if (goalwall == null){
            gauge.gaugeInitialization();
            return;
        }
        goalpos = goalwall.transform.position.x;
        gauge.gaugeOpe(goalpos, length);
    }


    private void creatWall(){
        wallcnt++;
        prefabwall = (GameObject)Resources.Load("Wall");
        GameObject wall = Instantiate(prefabwall, container);

        int[] array = { 1, 0, -1 };
        int index = GetRandomIndexWithWeight(60, 10, 30);
        int result = array[index];

        float unit = 0.7f;

        float distance = (unit * result);
        if (direction == Direction.Down) distance *= -1;
        distance += previoius;
        if (distance < Min_Wall_Height){
            distance = Min_Wall_Height;
            direction = Direction.UP;
        }else if(distance > Max_Wall_Height){
            distance = Max_Wall_Height;
            direction = Direction.Down;
        }
           
        Vector3 wallvec = wall.transform.position;
        wallvec.y = distance;
        wallvec.x = Start_Wall_Position;
        wall.transform.position = wallvec;
        previoius = wall.transform.position.y;
        wall.SetActive(true);
        uewallsprite = wall.transform.GetChild(0).GetComponent<SpriteRenderer>();
        sitawallsprite = wall.transform.GetChild(1).GetComponent<SpriteRenderer>();
        int rnd = Random.Range(1, 6);
        switch(rnd){
            case 1: 
                uewallsprite.sprite = wall_color1;
                sitawallsprite.sprite = wall_color1;
                break;
            case 2:
                uewallsprite.sprite = wall_color2;
                sitawallsprite.sprite = wall_color2;
                break;
            case 3:
                uewallsprite.sprite = wall_color3;
                sitawallsprite.sprite = wall_color3;
                break;
            case 4:
                uewallsprite.sprite = wall_color4;
                sitawallsprite.sprite = wall_color4;
                break;
            case 5:
                uewallsprite.sprite = wall_color5;
                sitawallsprite.sprite = wall_color5;
                break;
            default:
                break;
        }


        if (wallcnt % 2 == 0){
            Vector3 starpos = wall.transform.position;
            GameObject star = Instantiate(prefabstar, container);

            float starunit = 1.0f;
            float stardis = starunit * result;
            starpos.y += stardis;
            star.transform.position = starpos;
            star.SetActive(true);
        }

    }

    private void createGoalWall(){
        prefabgoal = (GameObject)Resources.Load("Goal");
        goalwall = Instantiate(prefabgoal, container);
        Vector3 goalvec = goalwall.transform.position;
        float goalposx = Max_Wall_Count + 25;
        goalvec.x = goalposx;
        goalwall.transform.position = goalvec;
        length = goalvec.x;
        goalwall.SetActive(true);

    }

    public static int GetRandomIndexWithWeight(params int[] weightArray){
        int totalWeight = 0;
        for (int i = 0; i < weightArray.Length; i++){
            totalWeight += weightArray[i];
        }

        int randomValue = Random.Range(1, totalWeight + 1);
        int index = -1;
        for (var i = 0; i < weightArray.Length; ++i){
            if (weightArray[i] >= randomValue){
                index = i;
                break;
            }
            randomValue -= weightArray[i];
        }
        return index;
    }


    private IEnumerator waitTime(float time){
        var waittime = new WaitForSeconds(time);

        while (true){
            yield return waittime;
            creatWall();
            if (wallcnt == Max_Wall_Count){
                stopFunc();
            }
        }
    }

    public void stopFunc(){
        StopAllCoroutines();
    }

}
