﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KaisuDisplay : MonoBehaviour {

    private Text targetText;
    private float scale = 0.0f;

    private void Awake(){
        this.targetText = this.GetComponent<Text>();
    }
    //int で取得
    public void changeNum(string changenum) {
        //if (targetText.text == changenum) return;
        this.targetText.text = changenum;

        StartCoroutine(ActionNum());
    }

    IEnumerator ActionNum() {
        yield return StartCoroutine(ScalableFloatLerp(0.3f, (t) =>{
            scale = Mathf.Lerp(1.0f, 1.2f, t);
            this.transform.localScale = Vector3.one * scale;
        }));

        yield return StartCoroutine(ScalableFloatLerp(0.5f, (t) =>{
            scale = Mathf.Lerp(1.2f, 1.0f, t);
            this.transform.localScale = Vector3.one * scale;
        }));
    }


    static IEnumerator ScalableFloatLerp(float duration, Action<float> action) {
        float t = 0.0f;

        while (true)
        {
            t += Time.deltaTime;
            action.Invoke(t / duration);
            if (t > duration) break;
            yield return null;
        }

        action.Invoke(1.0f);
    }

}
