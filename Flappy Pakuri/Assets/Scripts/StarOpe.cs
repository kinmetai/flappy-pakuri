﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarOpe : MonoBehaviour {

    [SerializeField]
    GameObject stareffect = null;

    [SerializeField]
    Transform container = null;
 

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.gameObject.tag == "Player") {
            Vector3 starpos = this.gameObject.transform.position;
            GameObject effectprefab = (GameObject)Resources.Load("StarEffect");
            stareffect = Instantiate(effectprefab, container);
            stareffect.transform.position = starpos;
            stareffect.SetActive(true);
            stareffect.GetComponent<ParticleSystem>().Play();
            Destroy(this.transform.parent.gameObject);
            GameManeger.Instance.OnHitStar(true);
        }
    }

    void removeStar() {
        if(this.gameObject.transform.position.x >= 4.0f){
            Destroy(this.gameObject);
        }
        if(stareffect.gameObject.transform.position.x >= 4.0f){
            Destroy(this.stareffect);
        }
    }
}
